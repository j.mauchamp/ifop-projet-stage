﻿using IFOPAccountingIntergration.DAL;
using IFOPAccountingIntergration.Models;
using IFOPAccountingIntergration.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration
{
    class Program
    {
        static void Main(string[] args)
        {
            var gescomDb = Gescom.OpenDB();
            var files = FileUtils.GetFiles();
            foreach (var file in files)
            {                
                var pieces = AccountingPieces.GetPieces(file.FullName);
                foreach(var piece in pieces)
                {
                    BusinessLogic.AccountingPieceProcess.Create(gescomDb.CptaApplication, piece);
                }
            }
            Console.Read();
        }
    }
}
