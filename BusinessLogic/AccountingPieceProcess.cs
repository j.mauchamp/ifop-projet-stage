﻿using Objets100cLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Objets100cLib;
using IFOPAccountingIntergration.Models;

namespace IFOPAccountingIntergration.BusinessLogic
{
    class AccountingPieceProcess
    {
        public static void Create(BSCPTAApplication100c accountingDb, AccountingPiece piece)
        {
            var process = accountingDb.CreateProcess_Encoder();

            var journalCode = piece.JournalCode;
            var journal = (IBOJournal3)accountingDb.FactoryJournal.ReadNumero(journalCode);
            process.Date = piece.PieceDate;
            process.Journal = journal;
            process.EC_Piece = journal.NextEC_Piece[process.Date];
            process.EC_Reference = piece.Entries.First().Reference;

            var generalAccountLock = piece.Entries.First().GeneralAccountNumber;

            JournalLocking journalLocking = new JournalLocking(accountingDb, journalCode, process.Date, generalAccountLock);

            var journalAvailable = journalLocking.LockJournal();

            if (journalAvailable)
            {
                foreach (var entry in piece.Entries)
                {
                    var ecr = (IBOEcriture3)process.FactoryEcritureIn.Create();
                    var generalAccount = accountingDb.FactoryCompteG.ReadNumero(entry.GeneralAccountNumber);
                    ecr.CompteG = generalAccount;
                    ecr.EC_Intitule = entry.Label;
                    ecr.EC_Reference = entry.Reference;
                    ecr.EC_Montant = entry.DebitAmount != 0 ? entry.DebitAmount : entry.CreditAmount;
                    ecr.EC_Sens = entry.DebitAmount != 0 ? EcritureSensType.EcritureSensTypeDebit : EcritureSensType.EcritureSensTypeCredit;
                    if (entry.GeneralAccountNumber.StartsWith("40") && !string.IsNullOrEmpty(entry.ThirdPartyAccount))
                    {
                        ecr.Tiers = accountingDb.FactoryTiers.ReadNumero(entry.ThirdPartyAccount);
                        ecr.EC_Echeance = piece.PieceDate;
                    }
                    ecr.SetDefault();
                    ecr.WriteDefault();
                }

                if (process.CanProcess)
                {
                    process.Process();

                    var writedEntries = process.ListEcrituresOut;

                    foreach (IBOEcriture3 writedEntry in writedEntries)
                    {
                        var matchingEntry =
                            piece.Entries.First(e => e.GeneralAccountNumber == writedEntry.CompteG.CG_Num && (writedEntry.EC_Sens == EcritureSensType.EcritureSensTypeDebit ? e.DebitAmount == writedEntry.EC_Montant : e.CreditAmount == writedEntry.EC_Montant));
                        foreach (var analyticalEntry in matchingEntry.AnalyticalEntries)
                        {
                            var newAnalyticalEntry = (IBOEcritureA3)writedEntry.FactoryEcritureA.Create();
                            var plan = accountingDb.FactoryAnalytique.ReadIntitule(analyticalEntry.Plan);
                            var section = accountingDb.FactoryCompteA.ReadNumero(plan, analyticalEntry.Section);
                            newAnalyticalEntry.CompteA = section;
                            newAnalyticalEntry.EA_Montant = analyticalEntry.Amount;
                            newAnalyticalEntry.SetDefault();
                            newAnalyticalEntry.WriteDefault();
                        }
                    }
                }
                else
                {
                    var errors = process.Errors;
                    var textError = new StringBuilder();
                    foreach(IFailInfo error in errors)
                    {
                        textError.AppendLine($"indice : {error.Indice} - message : {error.Text}");
                    }
                    Console.WriteLine(textError.ToString());
                }
                journalLocking.UnlockJournal();
            }
        }
    }
}
