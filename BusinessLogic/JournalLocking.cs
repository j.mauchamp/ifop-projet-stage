﻿using System;
using Objets100cLib;

namespace IFOPAccountingIntergration.BusinessLogic
{

    public class JournalLocking
    {
        public JournalLocking(BSCPTAApplication100c _accountingDb, string _journalCode, DateTime _period,
            string _generalAccount)
        {
            try
            {
                accountingDb = _accountingDb;
                journal = accountingDb.FactoryJournal.ReadNumero(_journalCode);
                period = _period;
                generalAccount = accountingDb.FactoryCompteG.ReadNumero(_generalAccount);
            }
            catch (Exception e)
            {
                Console.WriteLine("JournalLocking", "Ctor", "Verrouillage du journal", "ERROR",
                    $"Echec de verrouillage du journal : {e.Message} - Trace : {e.StackTrace}", $"Journal : {journal.JO_Num} - Période : {period}");
                throw;
            }
        }

        public bool LockJournal()
        {
            try
            {
                var journalEntries = accountingDb.FactoryEcriture.QueryJournalPeriode(journal, period);

                if (journalEntries != null && journalEntries.Count > 0)
                {
                    entry = (IBOEcriture3)journalEntries[1];
                    entry.CouldModified();
                    isFictiveEntry = false;
                }
                else
                {
                    entry = (IBOEcriture3)accountingDb.FactoryEcriture.Create();
                    entry.Journal = journal;
                    entry.CompteG = generalAccount;
                    entry.Date = period;
                    entry.EC_Intitule = "Ecriture fictive de verrouillage";
                    entry.EC_Montant = 0.01;
                    entry.EC_Sens = EcritureSensType.EcritureSensTypeDebit;
                    entry.EC_Piece = journal.NextEC_Piece[period];
                    entry.Write();
                    entry.CouldModified();
                    isFictiveEntry = true;
                }
                isJournalLock = true;
            }
            catch (Exception)
            {
                isJournalLock = false;
            }
            return isJournalLock;
        }

        public void UnlockJournal()
        {
            if (!isJournalLock) return;
            if (isFictiveEntry)
            {
                entry.Remove();
            }
            else
            {
                entry.Read();
            }
            isJournalLock = false;
        }

        private static BSCPTAApplication100c accountingDb;
        private static IBOJournal3 journal;
        private static DateTime period;
        private static IBOCompteG3 generalAccount;
        private static IBOEcriture3 entry;
        private static bool isFictiveEntry;
        private static bool isJournalLock;


    }
}
