﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.Utils
{
    class StringUtils
    {
        public static string DecodePassword(string password)
        {
            var base64EncodedBytes = Convert.FromBase64String(password);
            password = Encoding.UTF8.GetString(base64EncodedBytes);
            return password;
        }
        public static string GetDateToString()
        {
            return $"{DateTime.Now:yyyyMMddhhmmssffff}";
        }
    }
}
