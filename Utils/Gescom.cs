﻿using Objets100cLib;

namespace IFOPAccountingIntergration.Utils
{
    class Gescom
    {
        public static BSCIALApplication100c OpenDB()
        {
            var gescomDb = new BSCIALApplication100c();
            try
            {
                gescomDb.Name = Globals.APPCONFIG.GescomPath;
                gescomDb.Loggable.UserName = Globals.APPCONFIG.GescomUser;
                gescomDb.Loggable.UserPwd = Globals.APPCONFIG.GescomPassword;
                gescomDb.Open();
            }
            catch
            {
                //todo log
            }
            return gescomDb;
        }

    }
}
