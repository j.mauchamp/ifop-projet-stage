﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.Utils
{
    class FileUtils
    {
        public static FileInfo[] GetFiles()
        {
            _currentMethod = MethodBase.GetCurrentMethod().Name;

           // Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod, "Récupérations des fichiers à traiter", "START", "Début de la récupération des fichiers", string.Empty));

            var fileFormat = Globals.APPCONFIG.FileFormat;

            var files = GetFiles(new DirectoryInfo(ConfigurationManager.AppSettings.Get("rootPath")), fileFormat);
           // Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod, "Récupérations des fichiers à traiter", "SUCCESS", "Fin de la récupération des fichiers", $"Nombre de fichier : {files.Length}"));
            return files;
        }
        public static FileInfo[] GetFiles(DirectoryInfo path, string fileFormat)
        {
            _currentMethod = MethodBase.GetCurrentMethod().Name;
            var rootDir = path;
            return Browse(rootDir, fileFormat);
        }

        private static FileInfo[] Browse(DirectoryInfo root, string fileFormat)
        {
            FileInfo[] files = null;
            _currentMethod = MethodBase.GetCurrentMethod().Name;
            try
            {
                files = root.GetFiles(fileFormat);
                //Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod,
                //    "Obtenir les fichiers présents dans le répertoire spécifié", "OK", "Obtention des fichiers réussie",
                //    root.FullName));
            }
            catch (Exception e)
            {
                //Globals.LOG.AddEntry(new LogEntry(_currentClass, _currentMethod, "Obtenir le fichier présents dans le répertoire spécifié",
                //    "ERROR : " + e.Message + " - TRACE : " + e.StackTrace, "Echec de l'obtention du fichier",
                //    root.FullName));
            }
            return files;
        }


        public static void MoveToArchive(FileInfo file)
        {
            _currentMethod = MethodBase.GetCurrentMethod().Name;
            string fileName = file.Name;
            string extention = fileName.Substring(fileName.LastIndexOf('.'));
            fileName = fileName.Substring(0, fileName.LastIndexOf('.'));
            Directory.CreateDirectory($@"{Globals.APPCONFIG.ArchivePath}");
            File.Move(file.FullName, $@"{Globals.APPCONFIG.ArchivePath}\{fileName}_ARCHIVE_{StringUtils.GetDateToString()}{extention}");
        }

        private static readonly string _currentClass = typeof(FileUtils).FullName;
        private static string _currentMethod;

        public static DataSet DataReader(string fileFormat, string filePath)
        {
            DataSet ds = null;

            try
            {               
                FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);
                IExcelDataReader reader;

                switch (fileFormat)
                {
                    case "xls":
                        reader = ExcelReaderFactory.CreateBinaryReader(stream);
                        ds = reader.AsDataSet();
                        break;
                    case "xlsx":
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        ds = reader.AsDataSet();
                        break;
                    default:
                        reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                        ds = reader.AsDataSet();
                        break;
                }

                stream.Dispose();
            }
            catch (Exception e)
            {
                Console.WriteLine($"error : {e.Message}  trace : {e.StackTrace}");
            }

            return ds;
        }
    }
}
