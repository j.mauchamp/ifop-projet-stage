﻿using IFOPAccountingIntergration.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.DAL
{
    class AnalyticalEntries
    {
        public static void GetEntry(AccountingPiece accountingPiece, DataRow entry)
        {
            var analyticalEntry = new AnalyticalEntry();
            var debitAmount = double.Parse(string.IsNullOrEmpty(entry[8].ToString()) ? "0" : entry[8].ToString());
            var creditAmount = double.Parse(string.IsNullOrEmpty(entry[9].ToString()) ? "0" : entry[9].ToString());
            analyticalEntry.Amount = debitAmount != 0 ? debitAmount : creditAmount;
            analyticalEntry.Plan = entry[10].ToString();
            analyticalEntry.Section = entry[5].ToString();
            var generalEntry = accountingPiece.Entries.First(e => e.GeneralAccountNumber == entry[2].ToString());

            generalEntry.AnalyticalEntries.Add(analyticalEntry);
        }
    }
}
