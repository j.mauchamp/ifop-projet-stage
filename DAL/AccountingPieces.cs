﻿using IFOPAccountingIntergration.Models;
using System;
using System.Collections.Generic;
using IFOPAccountingIntergration.Utils;
using System.Data;
using System.Globalization;

namespace IFOPAccountingIntergration.DAL
{
    class AccountingPieces
    {
        public static List<AccountingPiece> GetPieces(string filePath)
        {
            var accountingPieces = new List<AccountingPiece>();
            var groupedLines = GetGroupedLines(filePath);
            foreach (var groupement in groupedLines)
            {
                var accountingPiece = new AccountingPiece();
                accountingPiece.JournalCode = groupement[0][0].ToString();
                accountingPiece.PieceDate = DateTime.ParseExact(string.IsNullOrEmpty(groupement[0][1].ToString()) ? "0" : groupement[0][1].ToString(), "ddMMyyyy", CultureInfo.InvariantCulture);
                foreach (var entry in groupement)
                {
                    if (entry[4].ToString().Trim() != "A")
                    {
                        DAL.AccountingEntries.GetEntry(accountingPiece, entry);
                    }
                    else
                    {
                        DAL.AnalyticalEntries.GetEntry(accountingPiece, entry);
                    }
                }
                accountingPieces.Add(accountingPiece);
            }
            return accountingPieces;
        }

        private static List<List<DataRow>> GetGroupedLines(string filePath)
        {
            var ds = FileUtils.DataReader("xlsx", filePath);
            var cumul = 0.00;
            var accountingPieces = new List<AccountingPiece>();
            var groupedLines = new List<List<DataRow>>();
            var groupement = new List<DataRow>();

            foreach (DataRow row in ds.Tables[0].Rows)
            {
                if (row != ds.Tables[0].Rows[0])
                {
                    var debitString = string.IsNullOrEmpty(row[8].ToString()) ? "0" : row[8].ToString();
                    var creditString = string.IsNullOrEmpty(row[9].ToString()) ? "0" : row[9].ToString();
                    var debitAmount = double.Parse(debitString);
                    var creditAmount = double.Parse(creditString);

                    if (row[4].ToString().Trim() != "A")
                    {
                        if (debitAmount != 0)
                        {
                            cumul += debitAmount;
                        }
                        else
                        {
                            cumul -= creditAmount;
                        }
                    }
                    groupement.Add(row);

                    if (cumul == 0)
                    {
                        groupedLines.Add(groupement);
                        groupement = new List<DataRow>();
                    }
                    Console.WriteLine(row[0] + " cumul = à " + cumul);
                }

            }
            return groupedLines;
        }
    }
}
