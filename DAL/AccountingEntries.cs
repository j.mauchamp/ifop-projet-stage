﻿using IFOPAccountingIntergration.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.DAL
{
    class AccountingEntries
    {
        public static void GetEntry(AccountingPiece accountingPiece, DataRow entry)
        {
            var accountingEntry = new AccountingEntry();
            accountingEntry.GeneralAccountNumber = entry[2].ToString();
            accountingEntry.ThirdPartyAccount = entry[3].ToString();
            accountingEntry.Reference = entry[6].ToString();
            accountingEntry.Label = entry[7].ToString();
            accountingEntry.DebitAmount = double.Parse(string.IsNullOrEmpty(entry[8].ToString()) ? "0" : entry[8].ToString());
            accountingEntry.CreditAmount = double.Parse(string.IsNullOrEmpty(entry[9].ToString()) ? "0" : entry[9].ToString());

            accountingPiece.Entries.Add(accountingEntry);
        }
    }
}
