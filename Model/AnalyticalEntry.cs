﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.Models
{
    public class AnalyticalEntry
    {
        public string Plan { get; set; }
        public string Section { get; set; }
        public double Amount { get; set; }
    }
}
