﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.Models
{
    public class AccountingPiece
    {
        public AccountingPiece()
        {
            Entries = new List<AccountingEntry>();
        }
        public string JournalCode { get; set; }
        public DateTime PieceDate { get; set; }
        public List<AccountingEntry> Entries { get; set; }

    }
}
