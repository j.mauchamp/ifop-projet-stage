﻿using IFOPAccountingIntergration.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IFOPAccountingIntergration.Models
{
    public class AccountingEntry
    {
        public AccountingEntry()
        { 
            AnalyticalEntries = new List<AnalyticalEntry>();  
        }
        public string GeneralAccountNumber { get; set; }
        public string ThirdPartyAccount { get; set; }
        public string Reference { get; set; }
        public string Label { get; set; }
        public double DebitAmount { get; set; }
        public double CreditAmount { get; set; }
        public List<AnalyticalEntry> AnalyticalEntries { get; set; }
    }
}
